﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextRPG
{
    class Warrior : ICharacter
    {
        public string Name { private set; get; }

        public int Hp { set; get; }

        public int Power { private set; get; }

        public int Defence { private set; get; }

        public int Luck { private set; get; }

        public Stance Stance { private set; get; }

        private Random _rand;

        public Warrior(string name, Stance stance)
        {
            Name = name;
            Stance = stance;
            Random rand = new Random();
            Hp = rand.Next(1000, 1250);
            Power = rand.Next();
            if (stance == Stance.Aggressive)
            {
                Power = rand.Next(50, 100);
                Defence = rand.Next(0, 25);
            }
            else if(Stance == Stance.Passive)
            {
                Power = rand.Next(1, 50);
                Defence = rand.Next(25, 50);
            }
        }

        public void Attack(ICharacter character)
        {
            Luck = _rand.Next(0, 100);
            int demage = Power + Luck;
            character.Hp -= demage*(100-(character.Defence/100));
        }

        public bool CheckHealth()
        {
            throw new NotImplementedException();
        }
    }
}
