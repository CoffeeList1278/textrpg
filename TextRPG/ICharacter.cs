﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextRPG
{
    enum Stance
    {
        Aggressive,
        Passive,


    }

    interface ICharacter
    {
        string Name { get; }
        int Hp { set;  get; }
        int Power { get; }
        int Defence { get; }
        int Luck { get; }

        Stance Stance { get; }

        void Attack(ICharacter character);

        bool CheckHealth();
    }
}
