﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextRPG
{
    class Program
    {
        static void Main()
        {
            ICharacter warrior = new Warrior("Belz", 50, 100);
            ICharacter boss = new Warrior("Boss", 1000, 10);

            Console.WriteLine("Welcome to my game, you face the horror of " + boss.Name);
            Console.WriteLine("What do you want to do? Choose from 'Attack','Shoot','Block'");

            while (true)
            {
                string actionFromUser = Console.ReadLine();
                switch (actionFromUser)
                {
                    case "Attack":
                        boss.GetHit(warrior.Melee());
                        break;
                    case "Shoot":
                        boss.GetHit(warrior.Ranged());
                        break;
                    case "Block":
                        break;
                    default:
                        Console.WriteLine("Try it once more! Choose from 'Attack','Shoot','Block'");
                        break;
                }
                if (boss.CheckHealth())
                {
                    Console.WriteLine("Continue with attacks! Boss is still alive. His health: " + boss.Hp);
                }
                else
                {
                    Console.WriteLine("Congratulation! You've won!");
                    break;
                }
            }
        }
    }
}
